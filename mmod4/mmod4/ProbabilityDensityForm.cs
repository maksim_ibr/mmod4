﻿using System.Linq;
using System.Windows.Forms;

namespace mmod4
{
    public partial class ProbabilityDensityForm : Form
    {
        public ProbabilityDensityForm()
        {
            InitializeComponent();

            ShowGraphics();
        }

        private void ShowGraphics()
        {
            InitChart();

            var probabilityDensity = Assessments.GetProbabilityDensity(Sp.A);
            foreach (var point in probabilityDensity)
            {
                if (probabilityDensity.First() == point || probabilityDensity.Last() == point)
                {
                    continue;
                }

                probDensityChart.Series[0].Points.AddXY(point.X, point.Y);
            }
        }

        private void InitChart()
        {
            probDensityChart.Series[0].XValueMember = "X";
            probDensityChart.Series[0].YValueMembers = "Y";
            probDensityChart.Series[0].IsVisibleInLegend = false;
            probDensityChart.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            probDensityChart.ChartAreas[0].Position.Auto = true;
        }
    }
}
