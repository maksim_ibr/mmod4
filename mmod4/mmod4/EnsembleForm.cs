﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace mmod4
{
    public partial class EnsembleForm : Form
    {
        private static int SpCount { get; } = 5;

        public EnsembleForm()
        {
            InitializeComponent();

            ShowGraphics();
        }

        private void ShowGraphics()
        {
            InitChart();

            var spCollection = new List<List<double>>();
            var vCollection = Sp.GenerateVCollection(Sp.EnsembleIterationCount);
            var timeCollection = Sp.GenerateTimeCollection(Sp.EnsembleIterationCount).ToList();
            for (var i = 0; i < SpCount; i++)
            {
                var sp = Sp.GenerateEnsembleSp(Sp.A, Sp.W, vCollection[i], timeCollection);
                spCollection.Add(sp);
            }

            var counter = 0;
            foreach (var sp in spCollection)
            {
                for (var i = 0; i < sp.Count; i++)
                {
                    ensembleChart.Series[counter].Points.AddXY(timeCollection[i], sp[i]);
                }

                counter++;
            }
        }

        private void InitChart()
        {
            for (var i = 0; i < SpCount; i++)
            {
                if (i > 0)
                {
                    ensembleChart.Series.Add($"SeriesName-{i}");
                }

                ensembleChart.Series[i].XValueMember = "X";
                ensembleChart.Series[i].YValueMembers = "Y";
                ensembleChart.Series[i].IsVisibleInLegend = false;
                ensembleChart.Series[i].ChartType = SeriesChartType.Spline;
                ensembleChart.ChartAreas[0].Position.Auto = true;
            }
        }
    }
}
