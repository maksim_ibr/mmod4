﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace mmod4
{
    public partial class CorrelationForm : Form
    {
        public int SpCount { get; } = 3000;

        public CorrelationForm()
        {
            InitializeComponent();

            ShowGraphics();
        }

        private void ShowGraphics()
        {
            InitChart();

            var timeCollection = new List<double>();
            var stepVal = 0.0;
            for (var i = 0; i < SpCount; i++)
            {
                timeCollection.Add(stepVal);
                stepVal += 0.01;
            }

            const int time1 = 0;
            var correlationCollection = new List<double>();
            foreach (var time in timeCollection)
            {
                correlationCollection.Add(Assessments.GetCorrelationT(Sp.A, Sp.W, time1, time));
            }


            for (var i = 0; i < timeCollection.Count; i++)
            {
                correlationChart.Series[0].Points.AddXY(i, correlationCollection[i]);
            }
        }

        private void InitChart()
        {
            correlationChart.Series[0].XValueMember = "X";
            correlationChart.Series[0].YValueMembers = "Y";
            correlationChart.Series[0].IsVisibleInLegend = false;
            correlationChart.Series[0].ChartType = SeriesChartType.Point;
            correlationChart.ChartAreas[0].Position.Auto = true;
        }
    }
}
