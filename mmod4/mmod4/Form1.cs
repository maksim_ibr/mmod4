﻿using System.Windows.Forms;

namespace mmod4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            var spForm = new SpForm();
            spForm.Show();
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            var ensembleForm = new EnsembleForm();
            ensembleForm.Show();
        }

        private void button3_Click(object sender, System.EventArgs e)
        {
            var sectionForm = new SectionForm();
            sectionForm.Show();
        }

        private void button4_Click(object sender, System.EventArgs e)
        {
            var probabilityDensityForm = new ProbabilityDensityForm();
            probabilityDensityForm.Show();
        }

        private void button5_Click(object sender, System.EventArgs e)
        {
            var correlationForm = new CorrelationForm();
            correlationForm.Show();
        }

        private void button6_Click(object sender, System.EventArgs e)
        {
            var moForm = new MoForm();
            moForm.Show();
        }
    }
}
