﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace mmod4
{
    public partial class MoForm : Form
    {
        private static int SpCount { get; } = 5;

        public MoForm()
        {
            InitializeComponent();

            ShowGraphics();
        }

        private void ShowGraphics()
        {
            InitChart();

            var spCollection = new List<List<double>>();
            var vCollection = Sp.GenerateVCollection(Sp.EnsembleIterationCount);
            var timeCollection = Sp.GenerateTimeCollection(Sp.EnsembleIterationCount).ToList();
            for (var i = 0; i < SpCount; i++)
            {
                var sp = Sp.GenerateEnsembleSp(Sp.A, Sp.W, vCollection[i], timeCollection);
                spCollection.Add(sp);
            }

            for (var i = 0; i < spCollection[0].Count; i++)
            {
                moChart.Series[0].Points.AddXY(timeCollection[i], Assessments.GetAvg(spCollection, i));
            }
        }

        private void InitChart()
        {
            moChart.Series[0].XValueMember = "X";
            moChart.Series[0].YValueMembers = "Y";
            moChart.Series[0].IsVisibleInLegend = false;
            moChart.Series[0].ChartType = SeriesChartType.Spline;
            moChart.ChartAreas[0].Position.Auto = true;
        }
    }
}
