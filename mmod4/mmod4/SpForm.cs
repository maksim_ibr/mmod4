﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace mmod4
{
    public partial class SpForm : Form
    {
        private static int SpCount { get; } = 1;

        public SpForm()
        {
            InitializeComponent();

            ShowGraphics();
        }

        private void ShowGraphics()
        {
            InitChart();

            var spCollection = new List<List<double>>();
            var timeCollection = Sp.GenerateTimeCollection(Sp.SpIterationCount).ToList();
            for (var i = 0; i < SpCount; i++)
            {
                var vCollection = Sp.GenerateVCollection(Sp.SpIterationCount);
                var sp = Sp.GenerateSp(Sp.A, Sp.W, timeCollection, vCollection);
                spCollection.Add(sp);
            }

            var counter = 0;
            foreach (var sp in spCollection)
            {
                for (var i = 0; i < sp.Count; i++)
                {
                    spChart.Series[counter].Points.AddXY(timeCollection[i], sp[i]);
                }

                counter++;
            }

            spChart.Series[counter].Color = Color.Black;
            spChart.Series[counter].BorderWidth = 5;
            spChart.Series[counter + 1].Color = Color.Black;
            spChart.Series[counter + 1].BorderWidth = 5;
            foreach (var time in timeCollection)
            {
                spChart.Series[counter].Points.AddXY(time, Sp.A);
                spChart.Series[counter + 1].Points.AddXY(time, -Sp.A);
            }
        }

        private void InitChart()
        {
            for (var i = 0; i < SpCount + 2; i++)
            {
                if (i > 0)
                {
                    spChart.Series.Add($"SeriesName-{i}");
                }

                spChart.Series[i].XValueMember = "X";
                spChart.Series[i].YValueMembers = "Y";
                spChart.Series[i].IsVisibleInLegend = false;
                spChart.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
                spChart.ChartAreas[0].Position.Auto = true;
            }
        }
    }
}
