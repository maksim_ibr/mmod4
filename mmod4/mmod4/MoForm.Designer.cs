﻿namespace mmod4
{
    partial class MoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.moChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.moChart)).BeginInit();
            this.SuspendLayout();
            // 
            // moChart
            // 
            chartArea1.Name = "ChartArea1";
            this.moChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.moChart.Legends.Add(legend1);
            this.moChart.Location = new System.Drawing.Point(12, 11);
            this.moChart.Name = "moChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.moChart.Series.Add(series1);
            this.moChart.Size = new System.Drawing.Size(1300, 800);
            this.moChart.TabIndex = 3;
            this.moChart.Text = "chart1";
            // 
            // MoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1324, 823);
            this.Controls.Add(this.moChart);
            this.Name = "MoForm";
            this.Text = "MoForm";
            ((System.ComponentModel.ISupportInitialize)(this.moChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart moChart;
    }
}