﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics.Distributions;

namespace mmod4
{
    public static class Sp
    {
        public static int SpIterationCount { get; } = 200;
        public static int EnsembleIterationCount { get; } = 1000;
        public static int SectionIterationCount { get; } = 100;

        public static double A { get; } = 1.5;
        public static double W { get; } = 3.0;

        public static List<double> GenerateTimeCollection(int count, double from = 0.0, double to = 25.0)
        {
            var timeCollection = new double[count];
            ContinuousUniform.Samples(timeCollection, from, to);
            var result = timeCollection.ToList();
            result.Sort();
            return result;
        }

        public static List<double> GenerateVCollection(int count, double from = -Math.PI, double to = Math.PI)
        {
            var vCollection = new double[count];
            ContinuousUniform.Samples(vCollection, from, to);
            var result = vCollection.ToList();

            return result;
        }

        public static List<double> GenerateSp(double a, double w, List<double> timeCollection,
            List<double> vCollection) => timeCollection.Select((t, i) => a * Math.Cos(w * t + vCollection[i])).ToList();

        public static List<double> GenerateEnsembleSp(double a, double w, double v, List<double> timeCollection)
        {
            var spCollection = new List<double>();
            foreach (var time in timeCollection)
            {
                var spValue = a * Math.Cos(w * time + v);
                spCollection.Add(spValue);
            }

            return spCollection;
        }

        public static Tuple<List<double>, List<double>> GenerateSectionSp(double a, double w, double t, List<double> vCollection)
        {
            var spCollection = new List<double>();
            foreach (var v in vCollection)
            {
                var spValue = a * Math.Cos(w * t + v);
                spCollection.Add(spValue);
            }

            return new Tuple<List<double>, List<double>>(vCollection, spCollection);
        }
    }
}
