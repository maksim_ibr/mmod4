﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace mmod4
{
    public static class Assessments
    {
        public static List<PointF> GetProbabilityDensity(double a)
        {
            var xWithStep = new List<double>();
            for (var i = -a; i < a; i += 0.01)
            {
                xWithStep.Add(i); 
            }

            var result = new List<PointF>();
            foreach (var currentX in xWithStep)
            {
                result.Add(new PointF((float) currentX,
                    (float) (1 / (Math.PI * Math.Sqrt(Math.Pow(a, 2) - Math.Pow(currentX, 2))))));
            }

            return  result;
        }

        public static double GetCorrelationT(double a, double w, double time1, double time2) =>
            0.5 * Math.Pow(a, 2) * Math.Cos(w * (time2 - time1));

        public static double GetAvg(List<List<double>> list, int i)
        {
            var result = 0.0;
            foreach (var l in list)
            {
                result += l[i];
            }

            return result / list.Count;
        }
    }
}
