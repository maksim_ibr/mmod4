﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace mmod4
{
    public partial class SectionForm : Form
    {
        private static int SpCount { get; } = 5;

        public SectionForm()
        {
            InitializeComponent();

            ShowGraphics();
        }

        private void ShowGraphics()
        {
            InitChart();

            var spCollection = new List<Tuple<List<double>, List<double>>>();
           
            var timeCollection = Sp.GenerateTimeCollection(Sp.SectionIterationCount).ToList();
            for (var i = 0; i < SpCount; i++)
            {
                var vCollection = Sp.GenerateVCollection(Sp.SectionIterationCount);
                vCollection.Sort();
                var sp = Sp.GenerateSectionSp(Sp.A, Sp.W, timeCollection[i], vCollection);
                spCollection.Add(sp);
            }

            var counter = 0;
            foreach (var sp in spCollection)
            {
                for (var i = 0; i < sp.Item1.Count; i++)
                {
                    sectionChart.Series[counter].Points.AddXY(sp.Item1[i], sp.Item2[i]);
                }

                counter++;
            }
        }

        private void InitChart()
        {
            for (var i = 0; i < SpCount; i++)
            {
                if (i > 0)
                {
                    sectionChart.Series.Add($"SeriesName-{i}");
                }

                sectionChart.Series[i].XValueMember = "X";
                sectionChart.Series[i].YValueMembers = "Y";
                sectionChart.Series[i].IsVisibleInLegend = false;
                sectionChart.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
                sectionChart.ChartAreas[0].Position.Auto = true;
            }
        }
    }
}
